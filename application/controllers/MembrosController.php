<?php

class MembrosController extends Zend_Controller_Action
{
	protected $_model;

	protected $_modelConjuntos;

	public function init()
	{

	}

	public function indexAction()
	{
		//passando para a variavel o objeto do modelo
		$model = $this->_getModel();

		//busca todos os membros
		$membros = $model->listarTodos();

		//pegando o numero de registros
		$rowsMembros = count($membros);
		//passando os valores da busca para a view
		$this->view->membros = $membros;
		$this->view->rowsMembros = $rowsMembros;


		//buscar somente obreiros
		$cargo1 = 'Outros';
		$cargo2 = 'Cooperador';
		$obreiros = $model->buscarObreiros($cargo1, $cargo2);
		$rowsObreiros = count($obreiros);
		$this->view->obreiros = $obreiros;
		$this->view->rowsObreiros = $rowsObreiros;
	}

	public function inserirAction()
	{
		//buscando os conjuntos para colocar no select
		$modelConjuntos = $this->_getModelConjuntos();

		$conjuntos = $modelConjuntos->listarConjuntos();

		$this->view->conjuntos = $conjuntos;

		if ($this->_request->isPost()) {
			$post = $this->_request->getPost();

			//dados do membro vindos do formulario
			//lado esquerdo coluna da tabela no BD e lado direito a propriedade name do form
			$data = array(
				"nome"					=> $post['nome'],
				"nome_pai"				=> $post['nome_pai'],
				"nome_mae"				=> $post['nome_mae'],
				"data_nascimento"		=> $post['data_nascimento'],
				"estado_civil"			=> $post['estado_civil'],
				"data_casamento"		=> $post['data_casamento'],
				"escolaridade"			=> $post['escolaridade'],
				"profissao"				=> $post['profissao'],
				"rg"					=> $post['rg'],
				"cpf"					=> $post['cpf'],
				"telefone"				=> $post['telefone'],
				"celular"				=> $post['celular'],
				"email"					=> $post['email'],
				"endereco"				=> $post['endereco'],
				"cep"					=> $post['cep'],
				"bairro"				=> $post['bairro'],
				"cidade"				=> $post['cidade'],
				"uf"					=> $post['uf'],
				"batismo_aguas"			=> $post['batismo_aguas'],
				"data_batismo_aguas"	=> $post['data_batismo_aguas'],
				"igreja_batismo"		=> $post['igreja_batismo'],
				"admitido"				=> $post['admitido'],
				"data_admicao"			=> $post['data_admicao'],
				"batismo_espirito"		=> $post['batismo_espirito'],
				"data_batismo_espirito"	=> $post['data_batismo_espirito'],
				"cargo_ministerial"		=> $post['cargo_ministerial'],
				"nome_conjuge"			=> $post['nome_conjuge'],
				"carteirinha"			=> $post['carteirinha'],
				"id_conjunto"			=> $post['id_conjunto'],
				"sexo"					=> $post['sexo'],
				"funcao"				=> $post['funcao'] 
			);

			$membro = $this->_getModel();

			$retornoMembro = $membro->save($data);

			return $this->_helper->redirector('index');
		}
	}

	public function editarAction()
	{
		//buscando os conjuntos para colocar no select
		$modelConjuntos = $this->_getModelConjuntos();

		$conjuntos = $modelConjuntos->listarConjuntos();

		$this->view->conjuntos = $conjuntos;

		//recebendo o id do membro pela url
		if ($this->_request->isGet()) {
			$id_membro = $this->_request->getParam('id');

			//buscando membro pra ser editado
			$model = $this->_getModel();

			$membro = $model->buscarMembro($id_membro);

			$this->view->membro = $membro;
		}
	}

	public function visualizarAction()
	{
		//pegando o valor do id_membro pela url
        if ($this->_request->isGet()) {
           	$id_membro = $this->_request->getParam('id');

           	$this->view->id = $id_membro;

           	//realizando a busca usando o id_membro como where
           	$model = $this->_getModel();

           	$retornoMembro = $model->buscarMembro($id_membro);

           	$this->view->retornoMembro = $retornoMembro;
        }

	}

	public function deleteAction()
	{
		//pegando o id do membro para efetuar o delete
		if ($this->_request->isGet()) {
			$id_membro = $this->_request->getParam('id');

			$model = $this->_getModel();

			$deleteMembro = $model->deleteMembro($id_membro);

			return $this->_helper->redirector('index');
		}
	}

	public function updateAction()
	{
		//recebendo pela url o id do membro para usar de where

		//recebebendo os dados via post para efetuar o update
		if ($this->_request->isPost()) {
			$post = $this->_request->getPost();

			//$id_membro = $post['id_membro'];

			$data = array(
				"nome"					=> $post['nome'],
				"nome_pai"				=> $post['nome_pai'],
				"nome_mae"				=> $post['nome_mae'],
				"data_nascimento"		=> $post['data_nascimento'],
				"estado_civil"			=> $post['estado_civil'],
				"data_casamento"		=> $post['data_casamento'],
				"escolaridade"			=> $post['escolaridade'],
				"profissao"				=> $post['profissao'],
				"rg"					=> $post['rg'],
				"cpf"					=> $post['cpf'],
				"telefone"				=> $post['telefone'],
				"celular"				=> $post['celular'],
				"email"					=> $post['email'],
				"endereco"				=> $post['endereco'],
				"cep"					=> $post['cep'],
				"bairro"				=> $post['bairro'],
				"cidade"				=> $post['cidade'],
				"uf"					=> $post['uf'],
				"batismo_aguas"			=> $post['batismo_aguas'],
				"data_batismo_aguas"	=> $post['data_batismo_aguas'],
				"igreja_batismo"		=> $post['igreja_batismo'],
				"admitido"				=> $post['admitido'],
				"data_admicao"			=> $post['data_admicao'],
				"batismo_espirito"		=> $post['batismo_espirito'],
				"data_batismo_espirito"	=> $post['data_batismo_espirito'],
				"cargo_ministerial"		=> $post['cargo_ministerial'],
				"nome_conjuge"			=> $post['nome_conjuge'],
				"carteirinha"			=> $post['carteirinha'],
				"id_conjunto"			=> $post['id_conjunto'],
				"sexo"					=> $post['sexo'],
				"funcao"				=> $post['funcao']
			);

			$id_membro = $this->_request->getParam('id');

			$model = $this->_getModel();

			$update = $model->updateMembro($data, $id_membro);

			return $this->_helper->redirector('index');
		}
		
	}

	public function _getModel()
	{
		if (null === $this->_model) {
			require_once APPLICATION_PATH . '/models/Membros.php';
			$this->_model = new Model_Membros();
		}

		return $this->_model;
	}

	public function _getModelConjuntos()
	{
		if (null === $this->_modelConjuntos) {
			require_once APPLICATION_PATH . '/models/Conjuntos.php';
			$this->_modelConjuntos = new Model_Conjuntos();
		}

		return $this->_modelConjuntos;
	}
	
}