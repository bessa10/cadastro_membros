<?php

class ConjuntosController extends Zend_Controller_Action
{
	//objeto model conjuntos
	protected $_model;
	//objeto model membros
	protected $_modelMembros;

	public function init()
	{

	}

	public function indexAction()
	{
		//busca todos os conjuntos
		$model = $this->_getModel();
		//$conjuntos = $model->todosConjuntos();
		
		$conjuntos = $model->listarConjuntos();

		$this->view->conjuntos = $conjuntos;

	}

	public function inserirAction()
	{
		//busca os nomes e id dos membros
		$model = $this->_getModelMembros();
		$membros = $model->fetchEntries();

		//passando os valores da busca para a view
		$this->view->membros = $membros;


		//começando a parte de inserção do conjunto
		if ($this->_request->isPost()) {
			$post = $this->_request->getPost();

			//dados do conjunto vindo do formulario
			$data = array(
				"tipo"			=> $post['tipo'],
				"nome"			=> $post['nome_conjunto'],
				"id_membro"		=> $post['primeiro_coordenador'],
				"2_coord"		=> $post['segundo_coordenador'],
				"3_coord"		=> $post['terceiro_coordenador'],
				"1_reg"			=> $post['primeiro_regente'],
				"2_reg"			=> $post['segundo_regente'],
				"3_reg"			=> $post['terceiro_regente'],
				"tesoureiro"	=> $post['tesoureiro'],
				"mes_festiv"	=> $post['mes_festividade'],
				"data_festiv"	=> $post['data_festividade']
			);

			$conjunto = $this->_getModel();

			$retornoConjunto = $conjunto->save($data);

			return $this->_helper->redirector('index');
		}
	}

	public function editarAction()
	{
		
	}

	public function _getModel()
	{
		if (null === $this->_model) {
			require_once APPLICATION_PATH . '/models/Conjuntos.php';
			$this->_model = new Model_Conjuntos();
		}

		return $this->_model;
	}

	//pegando o model de membros para mostrar nos selects os nomes e id
	public function _getModelMembros()
	{
		if (null === $this->_modelMembros) {
			require_once APPLICATION_PATH . '/models/Membros.php';
			$this->_modelMembros = new Model_Membros();
		}

		return $this->_modelMembros;
	}

}