<?php
class Zend_View_Helper_Link extends Zend_View_Helper_Abstract
{
    public $module;
    public $controller;
    public $action;
    
    public function Link()
    {
        $zc = Zend_Controller_Front::getInstance();
        $this->baseUrl = $zc->getRequest()->getBaseUrl();
        $this->module = $zc->getRequest()->getModuleName();
        $this->controller = $zc->getRequest()->getControllerName();
        $this->action = $zc->getRequest()->getActionName();
        return $this;
    }
    
    public function module()
    {
        $link = $this->baseUrl."/".$this->module;
        return $link;
    }
    
    public function controller()
    {
        $link = $this->baseUrl."/".$this->controller;
        return $link;
    }
    
    public function action()
    {
        $link = $this->baseUrl."/".$this->controller."/".$this->action;
        return $link;
    }
}