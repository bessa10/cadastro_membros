<?php
class Model_DbTable_Abstract extends Zend_Db_Table_Abstract
{
    protected $_sql = null;
    
    protected $_where = null;
    
    protected $_model = null;
    
    protected $_colunms = array();
    
    protected $_required = false;
    
    protected $_description = array();
    
    protected $_descriptionAlias = array();

    public function __construct()
    {
        if($this->_description){
            foreach($this->_description as $table => $values){
                $this->_descriptionAlias[$values["alias"]] = $table;
            }
        }
        parent::__construct();
    }
    
    public static function getInstance() 
    {
        $className = get_called_class();
        return new $className();
    }
        
    public function setColumns(Array $columns)
    {
        $this->_columns = $columns;
        return $this;
    }
    
    public function setWhere($colum, $value = null)
    {        
        $this->_sql->where($colum, $value);
        if($value){
            $this->_where.= ($this->_where) ? " AND " : null;
            $this->_where.= $this->getAdapter()->quoteInto($colum, $value);
        }else{
            $this->_where.= ($this->_where) ? " AND " : null;
            $this->_where.= $colum;
        }
        return $this;
    }
    
    public function setOrWhere($colum, $value = null)
    {
        $this->_sql->orWhere($colum, $value);
        if($value){
            $this->_where.= ($this->_where) ? " OR " : null;
            $this->_where.= $this->getAdapter()->quoteInto($colum, $value);
        }else{
            $this->_where.= ($this->_where) ? " OR " : null;
            $this->_where.= $colum;
        }
        return $this;
    }
    
    public function setGroup($group){
        $this->_sql->group($group);
        return $this;
    }
    
    public function setOrder($order){
        $this->_sql->order($order);
        return $this;
    }
    
    public function setJoin($table, $relation, $columns = "*", $schema = null)
    {
        if(!is_array($columns))
            array_push($this->_columns, $columns );
        else
            $this->_colunms = array_merge($this->_colunms, $columns);
        $this->_sql->join($table, $relation, $columns, $schema);
        return $this;
    }
    
    public function setJoinLeft($table, $relation, $columns = "*", $schema = null)
    {
        if(!is_array($columns))
            array_push($this->_columns, $columns );
        else
            $this->_colunms = array_merge($this->_colunms, $columns);
        $this->_sql->joinLeft($table, $relation, $columns, $schema);
        return $this;
    }
   
    public function setJoinRight($table, $relation, $columns = "*", $schema = null)
    {
        if(!is_array($columns))
            array_push($this->_columns, $columns );
        else
            $this->_colunms = array_merge($this->_colunms, $columns);
        $this->_sql->joinRight($table, $relation, $columns, $schema);
        return $this;
    }
    
    public function setJoinFull($table, $relation, $columns = "*", $schema = null)
    {
        if(!is_array($columns))
            array_push($this->_columns, $columns );
        else
            $this->_colunms = array_merge($this->_colunms, $columns);
        $this->_sql->joinFull($table, $relation, $columns, $schema);
        return $this;
    }
    
    public function setJoinCros($table, $columns = "*", $schema = null)
    {
        if(!is_array($columns))
            array_push($this->_columns, $columns );
        else
            $this->_colunms = array_merge($this->_colunms, $columns);
        $this->_sql->joinCross($table, $columns, $schema);
        return $this;
    }
    
    public function setLimit($length, $offSet = null)
    {        
        if($offSet){
            $this->_sql->limit($length, $offSet);
        }else{
            $this->_sql->limit($length);
        }
        return $this;
    }
    
    public function setRequired($bool = true)
    {
        $this->_required = $bool;
        return $this;
    }
    
    public function getAll(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $cloneSql = clone $this->_sql;
        $cloneSql->columns($this->_columns);
        return $this->getAdapter()->fetchAll($cloneSql);
    }
    
    public function getRow(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $cloneSql = clone $this->_sql;
        $cloneSql->columns($this->_columns);
        return $this->getAdapter()->fetchRow($cloneSql);
    }
    
    public function insert(Array $columns)
    {
        if($this->_name)
            return parent::insert($columns);
        return false;
    }
    
    public function update(Array $columns, $where = null)
    {  
        if($where)
            $this->_where = $where;
        if($this->_where)
            if($this->_name)
                return parent::update($columns, $this->_where);            
        return false;
    }
    
    public function delete($where = null)
    {
        if($where)
            $this->_where = $where;
        if($this->_where){
            if($this->_name)
                return parent::delete($this->_where);
        }
        return false;
    }
    
    public function getSql(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $cloneSql = clone $this->_sql;
        return $cloneSql->columns($this->_columns);
    }
    
    public function __toString() 
    {
        $this->_sql->columns($this->_columns);
        return "$this->_sql";
    }
    
    public function getAlias($table)
    {
        return $this->_description[$table]["alias"];
    }
   
    public function __call($table, $arguments) 
    {
        if (count ( $arguments ) > 0) {
            foreach ( $arguments as $param ) {
                if ($this->_description [$table])
                    $this->_description [$table] = array_merge ( $this->_description [$table], $param );
                else
                    $this->_description [$table] = $param;
            }
        }
        if (isset ( $this->_description[$table] )){
            extract($this->_description[$table]);
        }elseif(isset($this->_descriptionAlias[$table])){
            $alias = $table;
            $table = $this->_descriptionAlias[$table];
            extract ( $this->_description [$table] );
        }
        
        if (is_string ( $columns )) {
            if(is_array($this->_columns))
                array_push ( $this->_columns, "{$alias}.{$columns}" );
        } elseif (is_array ( $columns )) {
            foreach ( $columns as $colum ) {
                if(is_array($this->_columns))
                    array_push ( $this->_columns, "{$alias}.{$colum}" );
            }
        }
        $refTable = $this->_description [$relation ["refTable"]] ["alias"];
        $relColum = $relation ["relColum"];
        $refColum = $relation ["refColum"];
        if ($this->_sql) {
            if ($this->_required)
                $this->_sql->join ( array (
                    $alias => $table 
                ), "{$alias}.{$relColum} = {$refTable}.{$refColum}", array () );
            else
                $this->_sql->joinLeft ( array (
                    $alias => $table 
                ), "{$alias}.{$relColum} = {$refTable}.{$refColum}", array () );
        } else {
            extract ( $this->_description [$table] );
            $this->_name = $table;
            if($primary){
                $this->_primary = $primary;
            }
            $this->_columns = array (
                "{$alias}.*" 
            );
            $this->_sql = $this->select ()->setIntegrityCheck ( false )->from ( array (
                $alias => $table 
            ), array());
        }
        return $this;
    }
    
    public function __destruct()
    {
    	$this->getAdapter()->closeConnection();
    }
}