<?php

class Application_Model_DbTable_Conjuntos extends Zend_Db_Table_Abstract
{
	//Nome da tabela do banco de dados projeto_zend
	protected $_name = 'conjuntos';
	protected $_primary = 'id_conjunto';

	//definindo a relação da tabela conjuntos com a tabela membros
	//vetor para criar a relação
	protected $_referenceMap = array(
		"coordenador" => array(
			'columns'		=> 'id_membro',
			'refTableClass' => 'Membros',
			'refColumns'	=> 'id_membro',
		)
	);
	
}