<?php
abstract class Model_Abstract
{
    protected $_sql;
    
    protected $_model;
    
    protected $_colunms = array();
    
    protected $_required = false;
    
    protected $_description = array();
    
    protected $_descriptionAlias = array();

    protected function __construct()
    {
        if($this->_description){
            foreach($this->_description as $table => $values){
                $this->_descriptionAlias[$values["alias"]] = $refTable;
            }
        }
    }
    
    public static function getInstance() 
    {
        $className = get_called_class();
        return new $className();
    }
        
    public function setColumns(Array $columns)
    {
        $this->_columns = $columns;
        return $this;
    }
    
    public function setWhere($colum, $value = null)
    {
        
        $this->_sql->where($colum, $value);
        return $this;
    }
    
    public function setOrWhere($colum, $value = null)
    {
        $this->_sql->orWhere($colum, $value);
        return $this;
    }
    
    public function setGroup($group){
        $this->_sql->group($group);
        return $this;
    }
    
    public function setOrder($order){
        $this->_sql->order($order);
        return $this;
    }
    
    public function setRequired($bool = true)
    {
        $this->_required = $bool;
        return $this;
    }
    
    public function getAll(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $this->_sql->columns($this->_columns);
        return $this->_model->getAdapter()->fetchAll($this->_sql); 
    }
    
    public function getRow(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $this->_sql->columns($this->_columns);
        return $this->_model->getAdapter()->fetchRow($this->_sql);
    }
    
    public function getSql(Array $columns = array())
    {
        if($columns)
            $this->_columns = $columns;
        $cloneSql = clone $this->_sql;
        return $cloneSql->columns($this->_columns);
    }
    
    public function __toString() 
    {
        $this->_sql->columns($this->_columns);
        return "$this->_sql";
    }
    
    public function getAlias($table)
    {
        return $this->_description[$table]["alias"];
    }
   
    public function __call($table, $arguments) 
    {
        if (count ( $arguments ) > 0) {
            foreach ( $arguments as $param ) {
                if ($this->_description [$table])
                    $this->_description [$table] = array_merge ( $this->_description [$table], $param );
                else
                    $this->_description [$table] = $param;
            }
        }
        if (isset ( $this->_description [$table] ))
            extract ( $this->_description [$table] );
        elseif(isset($this->_descriptionAlias[$table]))
            $table = $this->_descriptionAlias[$table];

        if (is_string ( $columns )) {
            array_push ( $this->_columns, "{$alias}.{$columns}" );
        } elseif (is_array ( $columns )) {
            foreach ( $columns as $colum ) {
                array_push ( $this->_columns, "{$alias}.{$colum}" );
            }
        }
        $refTable = $this->_description [$relation ["refTable"]] ["alias"];
        $relColum = $relation ["relColum"];
        $refColum = $relation ["refColum"];
        if ($this->_sql) {
            if ($this->_required)
                $this->_sql->join ( array (
                    $alias => $table 
                ), "{$alias}.{$relColum} = {$refTable}.{$refColum}", array () );
            else
                $this->_sql->joinLeft ( array (
                    $alias => $table 
                ), "{$alias}.{$relColum} = {$refTable}.{$refColum}", array () );
        } else {
            extract ( $this->_description [$table] );
            $this->_columns = array (
                "{$alias}.*" 
            );
            $this->_model = new Zend_Db_Table ( array (
                "name" => $table 
            ));
            $this->_sql = $this->_model->select ()->setIntegrityCheck ( false )->from ( array (
                $alias => $table 
            ), array());
        }
        return $this;
    }
}