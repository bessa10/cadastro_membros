<?php

class Model_Membros extends Zend_Db_Table_Abstract
{
	protected $_table;
	//recupera o objeto tabela

	protected $_name = 'membros';

	public function getTable()
	{
		//obrigar a aplicação a usar essa tabela
		if (null == $this->_table){
			//instanciando a classe DbTable membros
			$this->_table = new Application_Model_DbTable_Membros();
		}

		return $this->_table;
	}

	public function save(array $data)
	{
		//passando os dados da tabela para a variavel $table
		$table = $this->getTable();

		//passando as colunas que a tabela possui
		$fields = $table->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		return $table->insert($data);
	}

	public function fetchEntries()
	{
		//retornando um array com os dados
		return $this->getTable()->fetchAll('1')->toArray();
	}

	public function listarTodos()
	{
		$table = $this->getTable();
		//buscando todos registros
			$select = $this->select()
					->from($table)
					->order('nome ASC');
		
		return $this->fetchAll($select);			
	}


	public function buscarObreiros($cargo1, $cargo2)
	{
		$table = $this->getTable();

		$select = $this->select()
					->from($table)
					->where('cargo_ministerial != ?', $cargo1)
					->where('cargo_ministerial != ?', $cargo2)
					->order('nome ASC');

		return $this->fetchAll($select);			
	}

	public function buscarMembro($id_membro)
	{
		$table = $this->getTable();

		$select = $this->select()
					->from($table)
					->where('id_membro = ?', $id_membro);

		return $this->fetchAll($select);			
	}

	public function updateMembro(array $data, $id_membro)
	{
		$table = $this->getTable();

		$where = $table->getAdapter()->quoteInto('id_membro = ?', $id_membro);

		return $table->update($data, $where);
	}

	public function deleteMembro($id_membro)
	{
		$table = $this->getTable();

		$where = $table->getAdapter()->quoteInto('id_membro = ?', $id_membro);

		return $table->delete($where);
	}
}	