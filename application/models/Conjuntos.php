<?php

class Model_Conjuntos extends Zend_Db_Table_Abstract
{
	protected $_table;
	protected $_name = 'conjuntos';

	public function getTable()
	{
		//obrigar a aplicação a usar essa tabela
		if (null == $this->_table){
			//instanciando a classe DbTable membros
			$this->_table = new Application_Model_DbTable_Conjuntos();
		}

		return $this->_table;
	}

	public function save(array $data)
	{
		//passando os dados da tabela para a variavel $table
		$table = $this->getTable();

		//passando as colunas que a tabela possui
		$fields = $table->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		return $table->insert($data);
	}

	public function fetchEntries()
	{
		//retornando um array com os dados
		return $this->getTable()->fetchAll('1')->toArray();
	}

	public function listarConjuntos()
	{
		$table = $this->getTable();

		$select = $this->select()
					->from($table)
					->order('nome ASC');

		return $this->fetchAll($select);			
	}
}	