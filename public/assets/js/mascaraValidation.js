// máscaras dos campos de cadastro e edição
jQuery( function($){
    $(".maskFone").mask("(99) 9999-9999");
    $(".maskCel").mask("(99) 99999-9999");
    $(".maskCPF").mask("999.999.999-99");
    $(".maskRG").mask("99.999.999-9");
    $(".maskCEP").mask("99999-999");
});	